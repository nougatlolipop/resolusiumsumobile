<!DOCTYPE html>
<html>

<head>
    <title>Tutorial Membuat QR Code dengan jQuery - Anan Bahrul Khoir</title>
    <style>
        .centered {
            position: fixed;
            top: 50%;
            left: 50%;
            margin-top: -175px;
            margin-left: -175px;
        }
    </style>
</head>

<body>
    <!-- <textarea id="teks" rows="10" cols="50"></textarea> <br />
<button onclick="generate_qrcode()">Generate Teks</button> -->
    <!-- <div id="jam" class="centered"></div> -->
    <div id="qrcode" class="centered"></div>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jquery-qrcode-0.17.0.js"></script>

    <script type="text/javascript">
        window.setTimeout("waktu()", 5000);

        function waktu() {
            var tanggal = new Date();
            setTimeout("waktu()", 10000);
            var tahun = tanggal.getFullYear();
            var bulan = tanggal.getMonth() + 1;
            var hari = tanggal.getDate();
            var jam = tanggal.getHours();
            var menit = tanggal.getMinutes();
            var detik = tanggal.getSeconds();

            if (bulan < 10) {
                bulan = '0' + bulan;
            }

            if (hari < 10) {
                hari = '0' + hari;
            }

            if (jam < 10) {
                jam = '0' + jam;
            }

            if (menit < 10) {
                menit = '0' + menit;
            }

            if (detik < 10) {
                detik = '0' + detik;
            }


            var teks = tahun + "-" + bulan + "-" + hari + " " + jam + ":" + menit + ":" + detik;

            $('#qrcode canvas').remove();
            $('#qrcode').qrcode({
                // render method: 'canvas', 'image' or 'div'
                render: 'canvas',

                minVersion: 6,

                // error correction level: 'L', 'M', 'Q' or 'H'
                ecLevel: 'H',

                // size in pixel
                size: 400,

                // code color or image element
                fill: '#000',

                // background color or image element, null for transparent background
                background: null,

                // content
                // text: 'Kampus 3,Gedung B,' + teks,
                text: 'https://bit.ly/umsumobile',

                // corner radius relative to module width: 0.0 .. 0.5
                // radius: 0.5,

                // quiet zone in modules
                quiet: 1,

                // modes
                // 0: normal
                // 1: label strip
                // 2: label box
                // 3: image strip
                // 4: image box
                mode: 2,

                label: 'UMSU',
                fontname: 'Ubuntu',
                fontcolor: '#000',

                image: 'https://upload.wikimedia.org/wikipedia/commons/d/de/LOGO_Umsu.png'
            });
        }
    </script>
</body>

</html>